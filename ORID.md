### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	今天上午先是惯例进行了Code Review，我的代码存在很多问题，有功能还没有实现。然后学习了项目开发中角色分工的知识，分析了我们项目的目标群体等内容并用Elevator Speech向PO介绍，学习了User journey的知识并绘制它，不断加以改进。最后进行了Retro，对团队存在的问题进行了分析并得到了改正方案。

### R (Reflective): Please use one word to express your feelings about today's class.

​	陌生，困难

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	由于基础薄弱的问题，虽然我在学习前端知识的这周每天在不影响第二天课程的基础上尽全力学习到凌晨1点，但还是有些跟不上课程的进度。我对自己进行了深刻的反思与检讨，认识到了自己的不足，明白自己相比其他基础好的同学需要付出更多的努力才能补上自己的缺陷。<br/>
&emsp;在通过今天的学习后，我明白了BA、QA的基本职责，但对User Story的划分肯定还有不清楚的地方，对这方面的知识还需要多多理解多多学习。
### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	我决定在课余时间，只要有空就去找前后端项目自己做，只有多敲代码，多做项目，才能提升自己的基础能力与编码水平。
